# PhotonOS Vendor Repository
Contains hardware-agnostic PhotonOS components, in addition to Maru OS rootfs archives.

## Hardware Support
* Nintendo Switch (`icosa_sr`)

To add platforms for official support, submit a Merge Request adding a prebuilt rootfs archive under `prebuilts/$(PRODUCT_DEVICE)/`.

## Contributing
See the [main Maru OS repository](https://github.com/maruos/maruos) for Maru OS contributing information.

See the [LineageOS charter repository](https://github.com/LineageOS/charter) for LineageOS contributing information.

Feel free to submit Merge Requests for code review.

## License
[Apache 2.0](LICENSE)
